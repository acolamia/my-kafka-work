package vit.vetoshnikov.consumerpostgresql.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vit.vetoshnikov.consumerpostgresql.model.MessageEntity;
import vit.vetoshnikov.consumerpostgresql.repository.MessageRepository;

@Service
public class MessageService {
    private MessageRepository messageRepository;

    @Autowired
    public void setMessageRepository(final MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public MessageRepository getMessageRepository() {
        return messageRepository;
    }

    public void save(final MessageEntity messageEntity){
        if (messageEntity!=null){
            messageRepository.save(messageEntity);
        }
    }
}
