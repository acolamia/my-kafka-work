package vit.vetoshnikov.consumerpostgresql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vit.vetoshnikov.consumerpostgresql.model.MessageEntity;

@Repository
public interface MessageRepository extends JpaRepository<MessageEntity, String > {
}
