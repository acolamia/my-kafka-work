package vit.vetoshnikov.consumerpostgresql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerPostgresqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerPostgresqlApplication.class, args);
    }

}
