package vit.vetoshnikov.consumerpostgresql.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import vit.vetoshnikov.consumerpostgresql.model.MessageEntity;
import vit.vetoshnikov.consumerpostgresql.service.MessageService;

@Service
public class Consumer {
    private MessageService messageService;
    private final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);

    public MessageService getMessageService() {
        return messageService;
    }

    @Autowired
    public void setMessageService(final MessageService messageService) {
        this.messageService = messageService;
    }

    @KafkaListener(topics = "topic", groupId = "group_one", containerFactory = "kafkaListenerContainerFactory")
    public void consume(final MessageEntity messageEntity) {
        messageService.save(messageEntity);
        //выводим сообщение в sout в качестве проверки
        System.out.println("Сообщение : \"" + messageEntity.getMessage()+"\" записано в базу");
    }
}
