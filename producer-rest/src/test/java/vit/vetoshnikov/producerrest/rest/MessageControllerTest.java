package vit.vetoshnikov.producerrest.rest;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import vit.vetoshnikov.producerrest.model.MessageEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageControllerTest {
    private MessageEntity messageEntity = new MessageEntity();

    @Autowired
    MessageController controller;

    @Before
    public void init() {
        messageEntity.setMessage("test message");
    }

    @Test
    public void sendMessageToKafkaReturnOK() {
        ResponseEntity<?> isSended = controller.sendMessageToKafka(messageEntity);
        Assert.assertThat(isSended, CoreMatchers.is(ResponseEntity.ok().build()));
    }
}