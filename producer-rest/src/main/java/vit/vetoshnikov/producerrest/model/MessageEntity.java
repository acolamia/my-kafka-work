package vit.vetoshnikov.producerrest.model;

public class MessageEntity {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
