package vit.vetoshnikov.producerrest.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import vit.vetoshnikov.producerrest.model.MessageEntity;

@Service
public class Producer {

    private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);
    private static final String TOPIC = "topic";

    private KafkaTemplate<String, MessageEntity> kafkaTemplate;

    public KafkaTemplate<String, MessageEntity> getKafkaTemplate() {
        return kafkaTemplate;
    }

    @Autowired
    public void setKafkaTemplate(final KafkaTemplate<String, MessageEntity> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void send(final MessageEntity message) {
        //логируем сообщение в качестве проверки
        LOGGER.info(String.format("\n ===== Producing message in JSON ===== \n" + message + " : " + message.getMessage()));
        kafkaTemplate.send(TOPIC, message);
    }
}