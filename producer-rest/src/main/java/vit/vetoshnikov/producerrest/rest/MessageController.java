package vit.vetoshnikov.producerrest.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vit.vetoshnikov.producerrest.kafka.Producer;
import vit.vetoshnikov.producerrest.model.MessageEntity;

@RestController
public class MessageController {
    private Producer producer;

    public Producer getProducer() {
        return producer;
    }

    @Autowired
    public void setProducer(final Producer producer) {
        this.producer = producer;
    }

    @PostMapping(value = "/send")
    public ResponseEntity<?> sendMessageToKafka(@RequestBody final  MessageEntity messageEntity) {
        this.producer.send(messageEntity);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
